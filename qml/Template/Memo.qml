import QtQuick 2.2

Rectangle {
    id: root
    width: 320
    height: 480

    Board {
        id: board

        size: Math.min(root.width, root.height)
    }
}
