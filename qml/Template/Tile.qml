import QtQuick 2.2

Flipable {
    id: flipable
    width: size
    height: size

    property bool flipped: true
    property int number: fieldValue
    property int size: 30

    function hide() {
        flipable.scale = 0.0
    }

    front: Rectangle {
        width: flipable.width
        height: flipable.height
        border.width: 1
        border.color: "black"
        radius: 3

        Text {
            anchors.centerIn: parent
            text: flipable.number
        }
    }
    back: Rectangle {
        width: flipable.width
        height: flipable.height
        border.width: 1
        radius: 3
        border.color: "black"

        Text {
            anchors.centerIn: parent
            text: "?"
            font.pixelSize: 20
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            flipable.flipped = !flipable.flipped
            board.checkTile(flipable)
            //console.log("h");
        }
    }

    states: State {
        name: "back"
        when: flipable.flipped
        PropertyChanges { target: rotation; angle: 180 }
    }

    transform: Rotation {
        id: rotation
        origin.x: flipable.width/2
        origin.y: flipable.height/2
        axis.x: 0; axis.y: 1; axis.z: 0
        angle: 0
    }

    transitions: Transition {
        NumberAnimation { target: rotation; property: "angle"; duration: 200 }
    }

    Behavior on scale { NumberAnimation { duration: 100 } }
}
