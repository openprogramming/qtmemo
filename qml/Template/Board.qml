import QtQuick 2.2
import QtQuick.Layouts 1.1
import "MapHelper.js" as MapHelper

Rectangle {
    id: board
    width: size
    height: size

    property int size: 10
    property int tilesInRow: 10
    property int boardSize: 4

    property variant selectedTile: undefined

    Component.onCompleted: {
        MapHelper.generateMap(memoBoardModel, boardSize, boardSize)
    }

    function checkTile(tile) {
        if (selectedTile === undefined) {
            selectedTile = tile
        }
        else {
            if (selectedTile.number === tile.number && selectedTile !== tile) {
                selectedTile.hide()
                tile.hide()
            }
            else {
                selectedTile.flipped = true
                tile.flipped = true
            }

            selectedTile = undefined;
        }
    }

    ListModel {
        id: memoBoardModel
    }

    GridView {
        anchors.fill: parent
        cellWidth: width / boardSize;
        cellHeight: width / boardSize;

        model: memoBoardModel
        delegate: Tile { size: 40 }
    }
}
